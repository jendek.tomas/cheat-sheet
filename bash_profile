export PATH=~/.composer/vendor/bin:$PATH
alias sail=’bash vendor/bin/sail’


alias sail='bash vendor/bin/sail'


alias l='ls'
alias ll='ls -l'
alias la='ls -la'

alias ..='cd ..'

# Tell ls to be colourful
export CLICOLOR=1
export LSCOLORS=Exfxcxdxbxegedabagacad
 
# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'


export TERM="xterm-color"
PS1='\[\e[0;33m\]\u\[\e[0m\]@\[\e[0;32m\]\h\[\e[0m\]:\[\e[0;34m\]\w\[\e[0m\]\$ '

